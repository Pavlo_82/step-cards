const selectInfo = {
    selectName: {
        selectName: 'doctors',
        selectFilter: 'filter',
    },
    selectClasses: {
        selectClassesFormFloating: "form-floating",
        selectClassesFormSelect: "form-select",
    },
    selectAttributes: {
        selectAttributesAria: 'aria-label',
        selectAttributesAriaText: 'select doctor',
        selectAttributesSelect: 'selected',
        selectAttributesSelectText: 'true',
        selectAttributesValue1: 'value',
        selectAttributesValue1Text: '1',
        selectAttributesValue2Text: '2',
        selectAttributesValue3Text: '3',
        selectAttributesHtmlFor: 'htmlFor',
        selectAttributesHtmlForText: "floatingSelect"
    },
    selectedTextDoctors: {
        selectedTextDoctorsSelected: 'Выберите врача',
        selectedTextDoctorsCardio: 'Кардиолог',
        selectedTextDoctorsDentist: 'Стоматолог',
        selectedTextDoctorsTherapist: 'Терапевт',
    },
    selectId: {
        selectedDoctorsId: 'selectedDoctors',
        selectFilterId: 'selectedFilter',
    }
}

class Select {
    constructor({selectName, selectClasses, selectAttributes, selectedTextDoctors, selectId,}) {
        this.selectName = selectName
        this.selectClasses = selectClasses
        this.selectAttributes = selectAttributes
        this.selectedTextDoctors = selectedTextDoctors;
        this.selectId = selectId;
        this.createSelect()
    }

    createSelect() {
        const divSelect = document.createElement('div');
        const select = document.createElement('select');
        const optionSelected = document.createElement('option');
        const optionValue1 = document.createElement('option');
        const optionValue2 = document.createElement('option');
        const optionValue3 = document.createElement('option');
        const selectLabel = document.createElement('label');
        const {selectDoctors, selectFilter} = this.selectName;
        const {selectClassesFormFloating, selectClassesFormSelect,} = this.selectClasses
        const {
            selectAttributesAria,
            selectAttributesAriaText,
            selectAttributesSelect,
            selectAttributesSelectText,
            selectAttributesValue1,
            selectAttributesValue1Text,
            selectAttributesValue2Text,
            selectAttributesValue3Text,
            selectAttributesHtmlFor,
            selectAttributesHtmlForText,
        } = this.selectAttributes
        const {
            selectedTextDoctorsSelected,
            selectedTextDoctorsCardio,
            selectedTextDoctorsDentist,
            selectedTextDoctorsTherapist
        } = this.selectedTextDoctors
        const {selectedDoctorsId, selectFilterId} = this.selectId

        divSelect.classList.add(selectClassesFormFloating)

        select.classList.add(selectClassesFormSelect)
        if (selectDoctors === 'doctors') {
            select.id = selectedDoctorsId
        }
        if (selectFilter === 'filter') {
            select.id = selectFilterId
        }
        select.setAttribute(selectAttributesAria, selectAttributesAriaText)
        divSelect.appendChild(select)

        // for (let i = 0; i < this.selectedTextDoctors.length; i++) {
        //     const option = document.createElement('option');
        //     for (let j = 0; j < this.selectAttributes.length; j++) {
        //         option.setAttribute('selected', 'selected')
        //         optionSelected.innerHTML = this.doctors
        //         select.appendChild(optionSelected)
        //     }
        // } ???????????

        optionSelected.setAttribute(selectAttributesSelect, selectAttributesSelectText)
        optionSelected.innerHTML = selectedTextDoctorsSelected
        select.appendChild(optionSelected)

        optionValue1.setAttribute(selectAttributesValue1, selectAttributesValue1Text)
        optionValue1.innerHTML = selectedTextDoctorsCardio;
        optionSelected.appendChild(optionValue1)

        optionValue2.setAttribute(selectAttributesValue1, selectAttributesValue2Text)
        optionValue2.innerHTML = selectedTextDoctorsDentist;
        optionSelected.appendChild(optionValue2)

        optionValue3.setAttribute(selectAttributesValue1, selectAttributesValue3Text)
        optionValue3.innerHTML = selectedTextDoctorsTherapist;
        optionSelected.appendChild(optionValue3);

        selectLabel.setAttribute(selectAttributesHtmlFor, selectAttributesHtmlForText);
        selectLabel.innerHTML = selectedTextDoctorsSelected;
        divSelect.appendChild(selectLabel)

        return divSelect
    }
}

const selectDoctors = new Select(selectInfo)

export {selectDoctors}