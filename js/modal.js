import {formCreator} from '/js/form.js'

const modalElement = document.querySelector('#enter');

const modalInfo = {
    modalName: {
        headerModalName: 'Регистрационная форма',
        // doctorsModalName: 'Создание карточки',
    },
    modalClasses: {
        modalDialog: 'modal-dialog',
        modalContent: 'modal-content',
        modalHeader: 'modal-header',
        modalTitle: 'modal-title',
        modalClose: 'btn-close',
        modalBody: 'modal-body',
        modalFooter: 'modal-footer'
    },
    modalId: {
        modalHeaderId: 'modalEnter',
        // modalDoctorId: 'modalDoctor'
    },
    modalChildren: {
        form: formCreator.createForm()
    },
    modalAtribute: {
        atributeRole: 'role',
        textRole: 'document',
        atributeType: 'type',
        buttonType: 'button',
        atributeDismiss: 'data-bs-dismiss',
        textDismiss: 'modal',
        atributeLabel: 'aria-label',
        textLabel: 'Close',
    },
    modalParent: {
        modalEnter: modalElement,
        // modalDoctor: 'modalDoctorElement';
    }
}

class Modal {
    constructor({modalName, modalClasses, modalId, modalChildren, modalAtribute, modalParent}) {
        this.modalName = modalName;
        this.modalParent = modalParent;
        this.modalChildren = modalChildren;
        this.modalClasses = modalClasses;
        this.modalAtribute = modalAtribute;
        this.modalId = modalId;
        this.createModalWindow();
    }

    createModalWindow() {
        const modalDialogElem = document.createElement('div');
        const modalContentElem = document.createElement('div');
        const modalHeaderElem = document.createElement('div');
        const header = document.createElement('h5');
        const bt = document.createElement('button');
        const modalBodyElem = document.createElement('div');
        const modalFooterElem = document.createElement('div');
        const {
            modalDialog,
            modalContent,
            modalHeader,
            modalTitle,
            modalClose,
            modalBody,
            modalFooter
        } = this.modalClasses;
        const {
            atributeRole,
            textRole,
            atributeType,
            buttonType,
            atributeDismiss,
            textDismiss,
            atributeLabel,
            textLabel
        } = this.modalAtribute;
        const {modalHeaderId, modalDoctorId} = this.modalId;
        const {headerModalName, doctorsModalName} = this.modalName;
        const {form} = this.modalChildren;
        const {modalEnter} = this.modalParent;

        modalDialogElem.classList.add(modalDialog);
        modalDialogElem.setAttribute(atributeRole, textRole);
        if (headerModalName) {
            modalEnter.appendChild(modalDialogElem)
        }

        modalContentElem.classList.add(modalContent);
        modalDialogElem.appendChild(modalContentElem);

        modalHeaderElem.classList.add(modalHeader);
        modalContentElem.appendChild(modalHeaderElem);

        header.classList.add(modalTitle);
        if (headerModalName) {
            header.id = modalHeaderId;
            header.innerHTML = headerModalName;
        }
        // if (doctorsModalName) {
        //     header.id = modalDoctorId;
        //     header.innerHTML = doctorsModalName;
        // }
        modalHeaderElem.appendChild(header)

        bt.classList.add(modalClose);
        bt.setAttribute(atributeType, buttonType)
        bt.setAttribute(atributeDismiss, textDismiss);
        bt.setAttribute(atributeLabel, textLabel)
        modalHeaderElem.appendChild(bt);

        modalBodyElem.classList.add(modalBody);
        modalContentElem.appendChild(modalBodyElem);

        modalContentElem.appendChild(modalFooterElem)

        modalBodyElem.appendChild(form)

        modalFooterElem.classList.add(modalFooter)
        // this.childrenButton.forEach(item => modalFooter.appendChild(item))
    }
}

const modalEnter = new Modal(modalInfo);

export {modalEnter}