import {buttonSubmit, buttonCancel} from '/js/button.js'
import {inputEmail, inputPass} from '/js/input.js'
import {selectDoctors} from '/js/select.js'
import {textAreaVisitInfo} from '/js/textarea.js'
import {api} from 'js/API.js'

const formInfo = {
    formName: {
        formEnter: 'Registration Form',
        // formDoctor: 'Create card',
    },
    formEnterChildren: [
        inputEmail.createInput(),
        inputPass.createInput(),
    ],
    formEnterChildrenButton: [
        buttonCancel.createButtonCancel(),
        buttonSubmit.createButtonSubmit(),
    ],
    formDoctorEnter: {}
}

const handleLogin = ({name, password}) => {
    console.log("name, password", name, password);
};

const loginConfig = [
    {
        name: "name",
        type: "email",
        element: 'input'
    },
    {
        name: "password",
        type: "password",
        element: 'input'

    },
    {
        name: "comment",
        element: 'textarea'

    },
];

const doctorConfig = [
    {
        name: "name",
        type: "email",
    },
    {
        name: "password",
        type: "password",
    },
];

class FormCreator {
    static FieldsDictionary = {
        input: Input,
        select: Select,
        textarea: Textarea,
    };

    constructor({formName, formEnterChildren, formEnterChildrenButton}, {config, onSubmit}) {
        this.config = config;
        this.onSubmit = onSubmit;
        this.formName = formName;
        this.formEnterChildren = formEnterChildren;
        this.formEnterChildrenButton = formEnterChildrenButton;
        this.form = document.createElement('form');
        // this.fields = this.getInputs();
        this.createForm();
    }

    createForm() {
        // Object.values(this.fields).forEach((field) => {
        //     this.form.append(field.element);
        // });

        const {formEnter, formDoctor} = this.formName
        const [inputEmail, inputPass] = this.formEnterChildren
        const [buttonCancel, buttonSubmit,] = this.formEnterChildrenButton
        console.log(inputEmail)


        if (formEnter) {
            this.form.innerHTML = formEnter
        }
        // if (formDoctor) {
        //     this.form.innerHTML = formDoctor
        // }

        // this.formEnterChildren.forEach(item => {
        //     this.form.appendChild(item)
        // });
        //
        // this.formEnterChildrenButton.forEach(item => this.form.appendChild(item));

        // this.form.addEventListener("submit", this.handleSubmit.bind(this));

        this.form.appendChild(inputEmail);
        this.form.appendChild(inputPass);
        this.form.appendChild(buttonCancel)
        this.form.appendChild(buttonSubmit)
        return this.form
    }

    // getInputs() {
    //     const fields = this.config.reduce((result, fieldConfig) => {
    //         console.log("fieldConfig", fieldConfig);
    //
    //         result[fieldConfig.name] = new this.constructor.FieldsDictionary[fieldConfig.element]({
    //             name: fieldConfig.name,
    //             placeholder: fieldConfig.type,
    //         });
    //
    //         return result;
    //     }, {});
    //
    //     return fields;
    // }
    //
    // handleSubmit(event) {
    //     console.log("this", this);
    //     event.preventDefault();
    //
    //     const formData = Object.entries(this.fields).reduce(
    //         (result, [key, input]) => {
    //             result[key] = input.element.value;
    //
    //             return result;
    //         },
    //         {}
    //     );
    //
    //     this.onSubmit(formData); // name, password
    // }
}

const formCreator = new FormCreator(formInfo, {config: loginConfig, onSubmit: handleLogin});

export {formCreator}