const buttonInfo = {
    buttonName: {
        buttonNameSubmit: 'Подтвердить',
        buttonNameCancel: 'Отменить',
    },
    buttonClasses: {
        buttonClassesBtn: "btn",
        buttonClassesBtnSecondary: "btn-secondary",
        buttonClassesBtnPrimary: 'btn-primary',
    },
    buttonAtribute: {
        buttonAttributeDismiss: 'data-bs-dismiss',
        buttonAttributeDismissText: 'modal',
        buttonAttributeType: 'type',
        buttonAttributeText: 'submit'
    },
    buttonId: {
        buttonIdSubmit: 'submit',
        buttonIdCancel: 'cancel',
    }
}

class Button {
    constructor({buttonName, buttonClasses, buttonAtribute, buttonId}) {
        this.buttonName = buttonName;
        this.buttonClasses = buttonClasses;
        this.buttonAtribute = buttonAtribute;
        this.buttonId = buttonId;
        this.createButtonCancel();
        this.createButtonSubmit()
    }

    createButtonCancel() {
        const button = document.createElement('button');
        const {buttonNameSubmit, buttonNameCancel,} = this.buttonName
        const {buttonClassesBtn, buttonClassesBtnSecondary, buttonClassesBtnPrimary,} = this.buttonClasses
        const {
            buttonAttributeDismiss,
            buttonAttributeDismissText,
            buttonAttributeType,
            buttonAttributeText
        } = this.buttonAtribute;
        const {buttonIdSubmit, buttonIdCancel,} = this.buttonId

        button.classList.add(buttonClassesBtn);
        button.classList.add(buttonClassesBtnPrimary);
        button.innerHTML = buttonNameSubmit;
        button.setAttribute(buttonAttributeType, buttonAttributeText);
        button.id = buttonIdSubmit

        return button
    }

    createButtonSubmit() {
        const button = document.createElement('button');
        const {buttonNameSubmit, buttonNameCancel,} = this.buttonName
        const {buttonClassesBtn, buttonClassesBtnSecondary, buttonClassesBtnPrimary,} = this.buttonClasses
        const {
            buttonAttributeDismiss,
            buttonAttributeDismissText,
            buttonAttributeType,
            buttonAttributeText
        } = this.buttonAtribute;
        const {buttonIdSubmit, buttonIdCancel,} = this.buttonId

        button.classList.add(buttonClassesBtn);
        button.classList.add(buttonClassesBtnSecondary);
        button.innerHTML = buttonNameCancel;
        button.setAttribute(buttonAttributeDismiss, buttonAttributeDismissText);
        button.id = buttonIdCancel

        return button
    }
}

const buttonCancel = new Button(buttonInfo)
const buttonSubmit = new Button(buttonInfo)

export {buttonCancel, buttonSubmit,}