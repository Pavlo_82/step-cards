const baseUrls = {
    baseUrl: 'https://ajax.test-danit.com/api/',
    baseUrlLogin: 'login',
    baseUrlCards: 'v2/cards',
}
const user = {
    email: 'ptopdruk@gmail.com',
    password: 'Pavlo04051982'
}


class API {
    constructor({baseUrl, baseUrlLogin, baseUrlCards}, {email, password}, patientInfo) {
        this.baseUrl = baseUrl;
        this.baseUrlLogin = baseUrlLogin;
        this.baseUrlCards = baseUrlCards;
        this.token = null;
        this.id = null;
        this.email = email;
        this.password = password;
        this.patientInfo = patientInfo
        this.getTokenLocalStorage();
        this.setTokenLocalStorage();
    }

    getTokenLocalStorage() {
        const token = localStorage.getItem('token');
        if (token) {
            this.token = token;
        }
        return token
    }


    async auth() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}${this.baseUrlLogin}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: this.email, password: this.password})
        })
        const token = await response.text()
        this.token = token;
        console.log(this.token)
        return token
    }

    setTokenLocalStorage() {
        if (this.token !== null) {
            localStorage.setItem('token', this.token)
        }
    }

    async getUrlRequest() {
        if (!this.token) {
            throw new Error('you are not authorized')
        }
        return await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
            headers: {
                Authorization: `Bearer ${this.token}`
            }
        })
    }

    async postRequest() {
        console.log(this.token)
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(this.patientInfo)
        })
        const responseData = await response.json()
        // console.log(responseData)
        return responseData
    }

    async getRequestAll() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
            method: 'Get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }

    async getRequestOne() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${this.id}`, {
            method: 'Get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }

    async putRequest() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${this.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(this.patientInfo)
        })
        const responseData = await response.json()
        return responseData
    }

    async deleteRequest() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${this.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }
}

const api = new API(baseUrls, user, patientInfo1)


export {api}