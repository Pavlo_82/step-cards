class Component {
    constructor(props) {
      this.props = { ...props };
    }
    
    createElement(tag, attr, content ='') {
      const element = document.createElement(tag);
      for (const [key, value] of Object.entries(attr)) {
        if (value) {
          element.setAttribute(key, value);
        }      
      }  
      if (content) {
        element.innerHTML = content;
    }
      return element;
    }
  }
  export default Component;



  class Element extends Component {
    render() {
        const { tag, content, handler, ...attr } = this.props;

        const element = this.createElement(tag, attr, content);
        this.element = element
        return element;
    }
    handler() {
        const { handler } = this.props;
        this.element.addEventListener(`${handler.event}`, handler.func)

    }
}
export default Element;


class ModalWindow extends Element {
    modal = {
        class: "modal-window",
    };
    closeBtn = {
        tag: "span",
        class: "",
        content: "&#10006",
        handler: {
            event: "click",
            func: function(e) {
                this.closest(".modal").style.display = "none";
                this.parentElement.remove()
                document.body.classList.remove('scroll-hidden');
            },
        },
    };


    render() {
        const { modal, closeBtn } = this;
        const modalWindow = this.createElement("div", modal);
        const close = new Element(closeBtn);

        modalWindow.append(
            close.render(),

        );
        close.handler();

        return modalWindow;
    }
}
export default ModalWindow;




class EditVisitModal extends Modal {
    title = {
        tag: "h3",
        class: "modal-title",
        content: "Изменить визит",
    };

    render(card) {
        const { title } = this;
        const modalWindow = super.render();

        modalWindow.append(new Element(title).render());

        modalWindow.classList.add("visit-modal");

        modalWindow.addEventListener("submit", async function editVisit(ev) {
            document.body.classList.remove('scroll-hidden');
            ev.preventDefault();
            let visit = {};
            visit.doctor = card.doctor;
            this.querySelectorAll("input, textarea, select").forEach((elem) => {
                visit[elem.name] = elem.value;
            });
            this.closest("#create-modal-window").style.display = "none";

            this.remove();
            const data = await editVisitOnBase(card.id, visit);

            document.getElementById(`${card.id}`).remove()

            renderCards(data.data);

        });
        return modalWindow;
    }
}
export default EditVisitModal;