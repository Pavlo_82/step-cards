
const inputInfo = {
    inputName: {
        inputNameEmail: 'mail',
        inputNamePass: 'pass'
    },
    inputPlaceholder: {
        inputPlaceholderEmail: 'Введите ваш е-мейл',
        inputPlaceholderPass: 'Введите ваш пароль'
    },
    inputClasses: {
        inputClassesMb3: 'mb-3',
        inputClassesCol: 'col-form-label',
        inputClassesFormControl: 'form-control',
        inputClassPass: 'password'
    },
    inputId: {
        inputIdMail: 'email',
        InputIdPass: 'password',
    },
    inputAtribute:
        {
            atributeFor: 'for',
            textFor: 'text',
            atributeType: 'type',
            textType: 'password',
        }
}

class Input {
    constructor({inputName, inputPlaceholder, inputClasses, inputId, inputAtribute}) {
        this.inputName = inputName;
        this.inputPlaceholder = inputPlaceholder;
        this.inputClasses = inputClasses;
        this.inputAtribute = inputAtribute;
        this.inputId = inputId;
        this.createInput()
    }

    createInput() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputNameEmail, inputNamePass} = this.inputName;
        const {inputPlaceholderEmail, inputPlaceholderPass} = this.inputPlaceholder;
        const {inputClassesMb3, inputClassesCol, inputClassesFormControl, inputClassPass} = this.inputClasses
        const {inputIdMail, inputIdPass,} = this.inputId
        const {atributeFor, atributeForText, atributeType, atributeTypeText,} = this.inputAtribute


        formDiv.classList.add(inputClassesMb3)


        formLabel.classList.add(inputClassesCol)
        formLabel.setAttribute(atributeFor, atributeForText)
        if (inputNamePass === 'password') {
            formLabel.innerHTML = inputPlaceholderPass;
            formLabel.classList.add(inputClassPass);
            formInput.id = inputIdPass;
        }
        if (inputNameEmail === 'email') {
            formLabel.innerHTML = inputPlaceholderEmail;
            formInput.id = inputIdMail
        }
        formDiv.appendChild(formLabel)

        formInput.classList.add(inputClassesFormControl)
        formInput.setAttribute(atributeType, atributeTypeText);
        formDiv.appendChild(formInput)

        return formDiv
    }
}

const inputEmail = new Input(inputInfo)
const inputPass = new Input(inputInfo)


// const inputEmail = new Input();
// const inputPass = new Input();
// const inputPurposeOfVisit = new Input();
// const inputUsualHeartPressure = new Input();
// const inputIndexBodyMass = new Input();
// const inputAge = new Input();
// const inputFullName = new Input();
// const inputLastVisit = new Input();

export {inputEmail, inputPass}