const textareaInfo = {
    textareaName: {
        texteraeNameVisit: 'Краткое опсание визита',
        texteraeNameVisitCardio: 'Перенесенные заболевания сердечно-сосудистой системы',
    },
    textAreaClasses: {
        textAreaClassFormFloating: "form-floating",
        textAreaClassFormControl: "form-control",
    },
    textAreaAttributes: {
        textAreaAttributesPlaceholder: 'placeholder',
        textAreaAttributesTextVisits: 'краткое описание визита',
        textAreaAttributesTextVisitCardio: 'перенесенные заболевания сердечно-сосудистой системы',
        // text: 'comments',
        textAreaAttributesHtmlForm: 'htmlFor',
        textAreaAttributesHtmlFormText: "floatingSelect"
    },
    textareaId: {
        textareaNameVisitId: 'textareaNameVisit',
        textareaNameVisitCardioId: 'textareaNameVisitCardio',
    }
};

class Textarea {
    constructor({textareaName, textAreaClasses, textAreaAttributes, textareaId,}) {
        this.textareaName = textareaName;
        this.textAreaClasses = textAreaClasses
        this.textAreaAttributes = textAreaAttributes
        this.textareaId = textareaId
        this.createTextArea()
    }

    createTextArea() {
        const divTextArea = document.createElement('div');
        const textArea = document.createElement('textarea');
        const textAreaLabel = document.createElement('label');
        const {textareaNameVisit, textareaNameVisitCardio} = this.textareaName
        const {textAreaClassFormFloating, textAreaClassFormControl} = this.textAreaClasses
        const {
            textAreaAttributesPlaceholder,
            textAreaAttributesTextVisits,
            textAreaAttributesTextVisitCardio,
            textAreaAttributesHtmlForm,
            textAreaAttributesHtmlFormText
        } = this.textAreaAttributes
        const {textareaNameVisitId, textareaNameVisitCardioId} = this.textareaId

        divTextArea.classList.add(textAreaClassFormFloating)

        textArea.classList.add(textAreaClassFormControl)

        if (textareaNameVisitCardio === 'cardio') {
            textArea.setAttribute(textAreaAttributesPlaceholder, textAreaAttributesTextVisitCardio)
            textArea.id = textareaNameVisitCardioId
        }
        textArea.setAttribute(textAreaAttributesPlaceholder, textAreaAttributesTextVisits)
        textArea.id = textareaNameVisitId

        divTextArea.appendChild(textArea)

        textAreaLabel.setAttribute(textAreaAttributesHtmlForm, textAreaAttributesHtmlFormText);

        if (textareaNameVisitCardio === 'cardio') {
            textAreaLabel.innerHTML = textAreaAttributesTextVisitCardio;
        }

        textAreaLabel.innerHTML = textAreaAttributesTextVisits;
        divTextArea.appendChild(textAreaLabel)

        return divTextArea
    }
}

const textAreaVisitInfo = new Textarea(textareaInfo)

// export {textAreaVisitInfo}

